The three *png* images available in the present directory are distributed following the [Creative Commons Attribution License](https://creativecommons.org/licenses/by/4.0/) as explicitly stated on the [webpage of the article](https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1008589).

According to the same page, the source to be cited is:
```
Masson J-B, Laurent F, Cardona A, Barré C, Skatchkovsky N, Zlatic M, et al. (2020) Identifying neural substrates of competitive interactions and sequence transitions during mechanosensory responses in Drosophila. PLoS Genet 16(2): e1008589. https://doi.org/10.1371/journal.pgen.1008589
```

The *html* files can be viewed [there](https://larvataggerpipelines.gitlab.io/t5_analysis_replicates/index.html).
