### A Pluto.jl notebook ###
# v0.19.35

using Markdown
using InteractiveUtils

# ╔═╡ b5386e86-474f-11ee-2fb3-8fb9d9070d7a
begin
	project_root = dirname(Base.current_project())
	
	import Pkg
	Pkg.activate(Base.current_project())
	Pkg.instantiate()

	include(joinpath(project_root, "src", "make_dataset.jl"))

	using PlanarLarvae, PlanarLarvae.Formats
	import PlanarLarvae.MWT: interpolate
	using Plots
	
end

# ╔═╡ 9f819fd2-b47d-4f7c-bb20-fe9ded5f1b31
begin
	datadir = joinpath(project_root, "data", "interim")
	datadir′= joinpath(project_root, "data", "interim", "trx")
	model = "20230311"
end;

# ╔═╡ 4fbf3b7a-d46b-4af9-9d8f-29c1f26a9943
begin
	lines = ("attP2", "iav")
	lines′= crossingname.(lines)
	allactions = model == "20230311" ? (:run_strong, :cast_strong, :stop_strong, :hunch_strong, :back_strong, :roll_strong, :run_weak, :cast_weak, :stop_weak, :hunch_weak, :back_weak, :roll_weak) : (:run_large, :cast_large, :stop_large, :hunch_large, :back_large, :roll_large, :small_motion)
	actions = model == "20230311" ? (:run_strong, :cast_strong, :stop_strong, :hunch_strong, :back_strong, :roll_strong) : (:run_large, :cast_large, :stop_large, :hunch_large, :back_large, :roll_large)
	actionlabels = ["crawl", "bend", "stop", "hunch", "back-up", "roll"]
	actioncolors = [:black, :red, :green, :blue, :cyan, :yellow]
end;

# ╔═╡ 66f09967-5ee2-4b01-a9aa-7816fa43a451
function listlabelfiles(line, datadir=datadir)
	predictedlabelfile = join(("predicted.", ".label"), model)
	labelfiles = String[]
	dir = joinpath(datadir, line)
	if isdir(dir)
		for (parent, _, files) in walkdir(dir)
			if predictedlabelfile in files
				push!(labelfiles, joinpath(parent, predictedlabelfile))
			end
		end
	end
	return labelfiles
end

# ╔═╡ 26acf8fd-559a-4ab2-8f8d-e09a0a9c9876
function getprobabilities(filelist;
	window=(40, 60), anchortime=45, frameinterval=.1, actions=actions,
)
	if eltype(actions) === Symbol
		actions = string.(actions)
	end
	nactions = length(actions)
	nsteps = round(Int, (window[2] - window[1]) / frameinterval) + 1
	timegrid = collect(range(window[1], window[2]; step=frameinterval))
	actioncounts = zeros(Int, nactions, nsteps)
	for file in filelist
		file = Formats.preload(file)
		if file isa Formats.Trxmat
			# load the behavior tags only
			Formats.drop_spines!(file)
			Formats.drop_outlines!(file)
			run = Formats.gettimeseries(file)
			for track in values(run)
				ts = [t for (t, _) in track]
				(ts[1] <= window[1] && window[2] <= ts[end]) || continue
				tags = [state for (_, state) in track]
				taggrid = interpolate(ts, tags, timegrid)
				for (col, step) in enumerate(taggrid)
					for action in convert(Vector{String}, step)
						row = findfirst(==(action), actions)
						if !isnothing(row)
							actioncounts[row, col] += 1
						end
					end
				end
			end
		elseif file isa Formats.JSONLabels
			run = Formats.getrun(file)
			for track in values(run)
				ts = track.timestamps
				(ts[1] <= window[1] && window[2] <= ts[end]) || continue
				tags = track[:labels]
				taggrid = interpolate(ts, tags, timegrid)
				for (col, step) in enumerate(taggrid)
					_, step = step
					for action in (step isa Vector ? step : [step])
						row = findfirst(==(action), actions)
						if !isnothing(row)
							actioncounts[row, col] += 1
						end
					end
				end
			end
		end
	end
	probabilities = actioncounts ./ sum(actioncounts, dims=1)
	return timegrid, probabilities
end

# ╔═╡ 537eab53-8a4d-4c7f-b76b-ec16a874647a
begin
	labelfiles = listlabelfiles(lines′[1])
	dirs = [relpath(dir, datadir) for dir in dirname.(labelfiles)]
	fullassays = [dir for dir in dirs if isfile(joinpath(datadir, dir, "groundtruth.label")) && isfile(joinpath(datadir′, dir, "predicted.$(model).label"))]
	predicted = [joinpath(datadir, dir, "predicted.$(model).label") for dir in fullassays]
	predicted′= [joinpath(datadir′, dir, "predicted.$(model).label") for dir in fullassays]
	timegrid, predicted_probabilities = getprobabilities(predicted)
	_, predicted_probabilities′ = getprobabilities(predicted′)
	groundtruth = [joinpath(datadir, dir, "groundtruth.label") for dir in fullassays]
	_, groundtruth_probabilities = getprobabilities(groundtruth)
end;

# ╔═╡ 4ddc7d64-59d3-4e62-99c5-4fdb5081e282
labelfiles

# ╔═╡ 5e8c7772-e6c8-4b6d-b773-bc4dfec14e02
begin
	function plotprobabilities(probabilities; ymax=0.72, kwargs...)
		p = vline([45];
			linewidth=2, linestyle=:dashdot, color=:purple,
			label="stimulus start", xlabel="time", ylabel="probability",
			kwargs...)
		ylims!(p, (0.0, ymax))
		for k in 1:size(probabilities, 1)
			plot!(p, timegrid, probabilities[k,:];
				label=actionlabels[k], linecolor=actioncolors[k], linewidth=2)
		end
		return p
	end
	p = plotprobabilities(groundtruth_probabilities; legend=false)
	q = plotprobabilities(predicted_probabilities)
	l = @layout [p q]
	plot(p, q, layout=l)
end

# ╔═╡ 5df2f364-ca0e-45b7-b8d3-1703806fbe78
begin
	p′ = plotprobabilities(groundtruth_probabilities; legend=false)
	q′ = plotprobabilities(predicted_probabilities′)
	l′ = @layout [p′ q′]
	p″ = plot(p′, q′, layout=l′, dpi=900)
	savefig("timeprofiles.png")
	p″
end

# ╔═╡ 6e04390b-6665-4adf-aa52-28b1f9a90989
function get_small_action_probabilities(files; allactions=allactions, actions=actions)
	smallactions = [a for a in allactions if a ∉ actions]
	if eltype(smallactions) === Symbol
		smallactions = string.(smallactions)
	end
	ntot = nsmall = 0
	for file in files
		file = Formats.preload(file)
		@assert file isa Formats.JSONLabels
		run = Formats.getrun(file)
		for track in values(run)
			for label in track[:labels]
				if label isa Vector
					if any(∈(smallactions), label)
						nsmall += 1
					end
				else
					if label in smallactions
						nsmall += 1
					end
				end
				ntot += 1
			end
		end
	end
	return nsmall / ntot
end

# ╔═╡ 7f37e4a5-e5be-40c8-8762-55350373e9a7
begin
	small_groundtruth = get_small_action_probabilities(groundtruth)
	small_predicted = get_small_action_probabilities(predicted)
	(small_groundtruth, small_predicted)
end

# ╔═╡ Cell order:
# ╟─b5386e86-474f-11ee-2fb3-8fb9d9070d7a
# ╠═9f819fd2-b47d-4f7c-bb20-fe9ded5f1b31
# ╠═4fbf3b7a-d46b-4af9-9d8f-29c1f26a9943
# ╟─66f09967-5ee2-4b01-a9aa-7816fa43a451
# ╟─26acf8fd-559a-4ab2-8f8d-e09a0a9c9876
# ╠═537eab53-8a4d-4c7f-b76b-ec16a874647a
# ╠═4ddc7d64-59d3-4e62-99c5-4fdb5081e282
# ╠═5e8c7772-e6c8-4b6d-b773-bc4dfec14e02
# ╠═5df2f364-ca0e-45b7-b8d3-1703806fbe78
# ╟─6e04390b-6665-4adf-aa52-28b1f9a90989
# ╠═7f37e4a5-e5be-40c8-8762-55350373e9a7
