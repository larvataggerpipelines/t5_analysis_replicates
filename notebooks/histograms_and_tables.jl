### A Pluto.jl notebook ###
# v0.19.35

using Markdown
using InteractiveUtils

# ╔═╡ 10772954-bcdb-11ed-02a9-f10984510626
begin
	project_root = dirname(Base.current_project())
	
	import Pkg
	Pkg.activate(Base.current_project())
	Pkg.instantiate()

	include(joinpath(project_root, "src", "make_dataset.jl"))

	using PlanarLarvae, PlanarLarvae.Formats
	using StatsBase, HypothesisTests, Plots
	using OrderedCollections: OrderedDict
	
end

# ╔═╡ 7644f870-e469-4cf9-92bf-85d29140f3e3
datadir = joinpath(project_root, "data", "interim");

# ╔═╡ 212563aa-5408-4e5d-8487-98f1b73d8938
fig1f_line = control = crossingname("attP2")

# ╔═╡ 4ca60d2f-b9bb-45f5-a70d-cc15356e6c01
model = "20230311"

# ╔═╡ b4b5a7d5-9100-4544-8a66-60c84dbf4015
function listlabelfiles(line)
	predictedlabelfile = join(("predicted.", ".label"), model)
	labelfiles = String[]
	dir = joinpath(datadir, line)
	if isdir(dir)
		for (parent, _, files) in walkdir(dir)
			if predictedlabelfile in files
				push!(labelfiles, joinpath(parent, predictedlabelfile))
			end
		end
	end
	return labelfiles
end

# ╔═╡ d5646f1c-353d-48fb-8e9b-097fdc3ce1ee
begin
	allactions = model == "20230311" ? (:run_strong, :cast_strong, :stop_strong, :hunch_strong, :back_strong, :roll_strong, :run_weak, :cast_weak, :stop_weak, :hunch_weak, :back_weak, :roll_weak) : (:run_large, :cast_large, :stop_large, :hunch_large, :back_large, :roll_large, :small_motion)
	newcounter(actions=allactions) = OrderedDict((action=>0 for action in actions)...)
end

# ╔═╡ 17d71f68-8512-436a-84e5-4324d605f04b
begin
	Window = Tuple{Number, Number}
	function countactions!(counter::AbstractDict, file::String, window::Window)
		assay = load(file)
		for track in values(getrun(assay))
			t = times(track)
			i = searchsortedfirst(t, window[1])
			if 1 < i && t[i-1] < window[1]
				j = searchsortedfirst(t, window[2])
				if j <= length(t) && window[2] <= t[j]
					actions = track.states[:labels][i:j]
					for action in Symbol.(actions)
						if action in keys(counter)
							counter[action] += 1
						end
					end
				end
			end
		end
		counter
	end
	function countactions!(counter::AbstractDict, files::Vector{String}, window::Window)
		foreach(files) do file
			countactions!(counter, file, window)
		end
		counter
	end
	countactions(file, window) = countactions!(newcounter(), file, window)
end

# ╔═╡ 2bdce8ae-1917-40d6-a7f0-73a548390209
begin
	function countfile(line, window; prefix=nothing)
		isnothing(line) && return
		suffix = "histogram$(round(Int, window[2]-window[1]))s.$(model).csv"
		joinpath(datadir, line, (isnothing(prefix) ? suffix : join((prefix, suffix), "_")))
	end
	function savecounts(line, window, counts; findfile=countfile, prefix=nothing)
		open(findfile(line, window; prefix=prefix),  "w") do f
			write(f, join(string.(keys(counts)), ",") * "\n")
			write(f, join(string.(values(counts)), ","))
		end
		counts
	end
	function loadcounts(line, window; findfile=countfile, prefix=nothing)
		open(findfile(line, window; prefix=prefix), "r") do f
			k = Symbol.(split(readline(f), ","))
			v = parse.(Int, split(readline(f), ","))
			OrderedDict(zip(k, v))
		end
	end
	function getcounts(line, window; prefix=nothing)
		if isnothing(line)
			return
		elseif isfile(countfile(line, window; prefix=prefix))
			loadcounts(line, window; prefix=prefix)
		else
			labelfiles = listlabelfiles(line)
			isempty(labelfiles) && return
			counts = countactions(labelfiles, window)
			savecounts(line, window, counts; prefix=prefix)
		end
	end
end

# ╔═╡ 81136726-b3ed-4dc0-88d0-fb7694c25cc8
begin
	function cumulativecounts!(counter::AbstractDict, file::String, window::Window)
		assay = load(file)
		for track in values(getrun(assay))
			t = times(track)
			i = searchsortedfirst(t, window[1])
			if 1 < i && t[i-1] < window[1]
				j = searchsortedfirst(t, window[2])
				if j <= length(t) && window[2] <= t[j]
					actions = track.states[:labels][i:j]
					for action in keys(counter)
						if string(action) in actions
							counter[action] += 1
						end
					end
					counter[:total] += 1
				end
			end
		end
		counter
	end
	function cumulativecounts!(counter::AbstractDict, files::Vector{String}, window::Window)
		foreach(files) do file
			cumulativecounts!(counter, file, window)
		end
		counter
	end
	function newcumulativecounter()
		counter = newcounter()
		counter[:total] = 0
		counter
	end
	cumulativecounts(file, window) = cumulativecounts!(newcumulativecounter(), file, window)
	function cumulativecountfile(line, window; prefix=nothing)
		suffix = "cumulative$(round(Int, window[2]-window[1]))s.$(model).csv"
		joinpath(datadir, line, join((prefix, suffix), "_"))
	end
	function getcumulativecounts(line, window; prefix=nothing)
		if isfile(cumulativecountfile(line, window; prefix=prefix))
			loadcounts(line, window; findfile=cumulativecountfile, prefix=prefix)
		else
			counts = cumulativecounts(listlabelfiles(line), window)
			savecounts(line, window, counts; findfile=cumulativecountfile, prefix=prefix)
		end
	end
end

# ╔═╡ 5dd5ac98-efb9-4038-9ab5-c8875d5c2a82
function fig1f_plot(mat, tot; alphas=[1, .6, .3], colors=[:black, :red, :green, :blue, :cyan], labels=["crawl", "bend", "stop", "hunch", "back-up"], ymax=:auto)
	m, n = size(mat)
	hist = mat ./ tot
	dx = 1 / (m + 1)
	center(a) = a .- mean(a)
	minor = repeat(center(0:dx:(m-1)*dx), 1, n)
	major = repeat(transpose(0:n-1), m, 1)
	alpha = repeat(alphas[1:m], 1, n)
	xpos = minor .+ major
	bar(xpos, 100 * hist; fillcolor=repeat(reshape(colors[1:n], 1, n), 1, m), label=nothing, ylabel="Behavioral probability (%)", fillalpha=alpha)
	xticks!(0:n-1, labels)
	ylims!((0, ymax === :auto ? ceil(maximum(hist) * 10) * 10 : ymax))
end

# ╔═╡ 991862ea-0d96-4274-a40e-efade4bb8333
begin
	offset = 0
	windows = ((45, 46), (45, 50), (45, 60))
	actions = model == "20230311" ? (:run_strong, :cast_strong, :stop_strong, :hunch_strong, :back_strong) : (:run_large, :cast_large, :stop_large, :hunch_large, :back_large)
	actionlabels = ["crawl", "bend", "stop", "hunch", "back-up"]
	actioncolors = [:black, :red, :green, :blue, :cyan]
	winalphas = [1, .6, .3]
	mat = zeros(Int, (length(windows), length(actions)))
	tot = zeros(Int, (length(windows), 1))
	for (i, window) in enumerate(windows)
		window = (window[1] + offset, window[2] + offset)
		counts = getcumulativecounts(fig1f_line, window)
		for (j, action) in enumerate(actions)
			mat[i,j] = get(counts, action, 0)
		end
		tot[i] = get(counts, :total, 0)
	end
	fig1f_plot(mat, tot)
	title!("Fig. 1F")
end

# ╔═╡ 22bc64aa-830a-4d5d-aa3d-acbf726fefde
begin
	baseline_windows = ((44, 45), (40, 45), (30, 45))
	baseline_mat = zeros(Int, (length(baseline_windows), length(actions)))
	baseline_tot = zeros(Int, (length(baseline_windows), 1))
	for (i, window) in enumerate(baseline_windows)
		window = (window[1] - offset, window[2] - offset)
		counts = getcumulativecounts(fig1f_line, window; prefix="baseline")
		for (j, action) in enumerate(actions)
			baseline_mat[i,j] = get(counts, action, 0)
		end
		baseline_tot[i] = get(counts, :total, 0)
	end
	fig1f_plot(baseline_mat, baseline_tot; ymax=100)
	title!("Before stimulus onset")
end

# ╔═╡ dfb18996-8dd3-4854-87fa-814b27836c79
begin
	fig3ab_lines = ["R61D08", "iav"]
	window = (45, 46)
	#
	window = (window[1] + offset, window[2] + offset)
	refcounts = getcounts(control, window)
	refmat = [get(refcounts, action, 0) for action in actions]
	reftot = sum(values(refcounts))
	#
	fig3ab_mat = zeros(Int, (length(fig3ab_lines), length(actions)))
	fig3ab_tot = zeros(Int, (length(fig3ab_lines), 1))
	for (i, line) in enumerate(fig3ab_lines)
		counts = getcounts(crossingname(line), window)
		for (j, action) in enumerate(actions)
			fig3ab_mat[i,j] = get(counts, action, 0)
		end
		fig3ab_tot[i] = sum(values(counts))
	end
	#
	for (i, line) in enumerate(fig3ab_lines)
		#@info ChisqTest(hcat(refmat, fig3ab_mat[i,:]))
		for (j, action) in enumerate(actions)
			t = ChisqTest([refmat[j]       (reftot - refmat[j]);
				    	   fig3ab_mat[i,j] (fig3ab_tot[i] - fig3ab_mat[i,j])])
			@info "$line - $action" pvalue=pvalue(t)
		end
	end
	#
	function fig3ab_plot(i)
		mat = transpose(hcat(refmat, fig3ab_mat[i,:]))
		fig1f_plot(mat, [reftot, fig3ab_tot[i]]; alphas=[1,.3], ymax=50)
		title!(fig3ab_lines[i])
	end
	fig3ab_plots = [fig3ab_plot(i) for i in 1:length(fig3ab_lines)]
	plot(fig3ab_plots...; layout=(1, length(fig3ab_plots)))
end

# ╔═╡ 603fb1d3-af0d-4d76-a683-4797e0364b30
begin
	actionlabels′ = Dict(
		:run_large=>"crawl",
		:cast_large=>"bend",
		:stop_large=>"stop",
		:hunch_large=>"hunch",
		:back_large=>"back-up",
		:roll_large=>"roll",
		:small_motion=>"small action",
		:run_strong=>"crawl",
		:cast_strong=>"bend",
		:stop_strong=>"stop",
		:hunch_strong=>"hunch",
		:back_strong=>"back-up",
		:roll_strong=>"roll",
	)
	actionlabel(action) = get(actionlabels′, action, string(action))
	
	struct Hit
		name::String
		differences::AbstractDict{String, String}
	end
	Hit(name::String) = Hit(name, OrderedDict{String, String}())
	Hit(name::String, actiondiff1::Pair{String, String}, otherdiffs...) = Hit(name, OrderedDict(actiondiff1, otherdiffs...))
	function Hit(name::String, actiondiff1::Pair{Symbol, String}, otherdiffs...)
		Hit(name, OrderedDict(actionlabel(actiondiff1),
							  actionlabel.(otherdiffs)...))
	end
	
	Base.getindex(hit::Hit, action) = hit.differences[action]
	Base.setindex!(hit::Hit, value, action) = (hit.differences[action] = value)
	Base.keys(hit::Hit) = keys(hit.differences)
	Base.values(hit::Hit) = values(hit.differences)
	
	function hittable(linelist, actions′)
		lines = readlines(joinpath(project_root, "data", linelist))
		mat = zeros(Int, (length(lines), length(actions′)))
		tot = zeros(Int, (length(lines), 1))
		for (i, line) in enumerate(lines)
			counts = getcounts(crossingname(line), window)
			isnothing(counts) && continue
			for (j, action) in enumerate(actions′)
				mat[i,j] = get(counts, action, 0)
			end
			tot[i] = sum(values(counts))
		end
		refmat′ = [get(refcounts, action, 0) for action in actions′]
		table = Hit[]
		for (i, line) in enumerate(lines)
			tot[i] == 00 && continue
			hit = Hit(line)
			for (j, action) in enumerate(actionlabel.(actions′))
				contingencytable = [refmat′[j] (reftot - refmat′[j]);
					    	        mat[i,j]   (tot[i] - mat[i,j])  ]
				if any(contingencytable .< 5)
					hit[action] = "x"
				else
					t = ChisqTest(contingencytable)
					s = mat[i,j]/tot[i] > refmat′[j]/reftot ? "+" : "-"
					pv = pvalue(t)
					hit[action] = pv<=.05 ? s * (pv<=0.01 ? s * (pv<=0.001 ? s : "") : "") : ""
				end
			end
			push!(table, hit)
		end
		table
	end
end

# ╔═╡ f1936c76-ad97-49b4-90c7-43b90958e1c8
md"""
The below 20230311 tagger generates labels for the 12-class behavior classification that it maps onto the 7-class behavior classification.
"""

# ╔═╡ 011024b8-4613-444e-8f41-05b6740fd08f
md"""
Similarly to the referred 1F figure, the different shades of a color corresponds to different window sizes: 1 second (darker), 5 seconds and 15 seconds (lighter).

Note: if you get a `SystemError` due to a missing .csv file, run `make import`.
"""

# ╔═╡ 73e3579d-7475-4359-900e-69289c37fd6e
begin
	struct Table
		th
		tr
		colored
	end
	Table(; th, colored=false) = Table(th, Vector{String}[], colored)

	effectsize_as_color = Dict(
		"---" => "#4472c4",
		"--"  => "#8ea9db",
		"-"   => "#bdd7ee",
		""    => "#ffffff",
		"+"   => "#ffc000",
		"++"  => "#ff7700",
		"+++" => "#ff0000",
	)
	
	function Base.show(io::IO, ::MIME"text/html", table::Table)
		if table.colored
			print(io,
				"""
				<style>
				table, th, td {
					color: black;
					background-color: white;
					border: 2px solid #e7e6e6;
				}
				</style>
				""")
		end
		print(io, "<table><th>")
		foreach(table.th) do td
			print(io, "<td>$td</td>")
		end
		print(io, "</th>")
		foreach(table.tr) do tr
			print(io, "<tr>")
			foreach(tr) do td
				print(io, begin
					if table.colored
						if td in keys(effectsize_as_color)
							color = effectsize_as_color[td]
							"<td style=\"background-color: $color\"></td>"
						else
							if td == "x"
								td = "NA"
							end
							"<td>$td</td>"
						end
					else
						"<td>$td</td>"
					end
				end)
			end
			print(io, "</tr>")
		end
		print(io, "</table>")
	end
	
	function htmlhittable(hits; colors=false)
		actions = keys(first(hits))
		table = Table(; th=actions, colored=colors)
		for hit in hits
			row = [hit.name]
			foreach(values(hit)) do td
				push!(row, td)
			end
			push!(table.tr, row)
		end
		table
	end
end

# ╔═╡ 87ece16c-84db-47f3-be74-45919be4b527
begin
	#actions′ = [actions..., :small_motion]
	actions′ = actions
	fig3e_hits = hittable("fig3e.txt", actions′)
	htmlhittable(fig3e_hits; colors=true)
end

# ╔═╡ 103637df-5d78-4b98-8ba7-f95ae9cd08ff
begin
	fig3f_hits = hittable("fig3f.txt", actions′)
	htmlhittable(fig3f_hits; colors=true)
end

# ╔═╡ 4d6b562b-7cc5-4a7b-b0fd-71808e604518
begin
	fig3g_hits = hittable("fig3g.txt", actions′)
	htmlhittable(fig3g_hits; colors=true)
end

# ╔═╡ da5d1748-71fb-47b0-9496-4a482ef67952
begin
	fig5a_hits = hittable("fig5a.txt", actions′)
	htmlhittable(fig5a_hits; colors=true)
end

# ╔═╡ 44ada27c-cf21-4e8b-875c-9ea649d8608d
begin
	fig3e_expected_hits = [
		Hit("iav", "crawl"=>"+++", "bend"=>"---", "stop"=>"+++", "hunch"=>"---", "back"=>"-"),
		Hit("R17B10", "crawl"=>"+++", "bend"=>"--", "stop"=>"+++", "hunch"=>"---", "back"=>"---"),
		Hit("R17F10", "crawl"=>"+++", "bend"=>"---", "stop"=>"", "hunch"=>"---", "back"=>"--"),
		Hit("R17G06", "crawl"=>"+++", "bend"=>"", "stop"=>"", "hunch"=>"---", "back"=>""),
		Hit("R18D09", "crawl"=>"+++", "bend"=>"---", "stop"=>"", "hunch"=>"---", "back"=>""),
		Hit("R23G02", "crawl"=>"+++", "bend"=>"", "stop"=>"+++", "hunch"=>"---", "back"=>"-"),
		Hit("R27B07", "crawl"=>"+++", "bend"=>"", "stop"=>"", "hunch"=>"---", "back"=>""),
		Hit("R46H11", "crawl"=>"+++", "bend"=>"", "stop"=>"-", "hunch"=>"---", "back"=>""),
		Hit("R49A06", "crawl"=>"+++", "bend"=>"---", "stop"=>"+++", "hunch"=>"---", "back"=>"---"),
		Hit("R52G08", "crawl"=>"+++", "bend"=>"---", "stop"=>"+++", "hunch"=>"---", "back"=>"---"),
		Hit("R53G05", "crawl"=>"+++", "bend"=>"---", "stop"=>"", "hunch"=>"---", "back"=>""),
		Hit("R61D08", "crawl"=>"+++", "bend"=>"---", "stop"=>"+++", "hunch"=>"---", "back"=>""),
		Hit("R72H06", "crawl"=>"+++", "bend"=>"---", "stop"=>"+++", "hunch"=>"---", "back"=>"---"),
		Hit("R73H07", "crawl"=>"+++", "bend"=>"---", "stop"=>"", "hunch"=>"", "back"=>"---"),
		Hit("R79F08", "crawl"=>"+++", "bend"=>"-", "stop"=>"", "hunch"=>"---", "back"=>""),
		Hit("R86D09", "crawl"=>"+++", "bend"=>"", "stop"=>"+++", "hunch"=>"---", "back"=>"-"),
		Hit("R30H09", "crawl"=>"+++", "bend"=>"---", "stop"=>"+++", "hunch"=>"---", "back"=>"---"),
		Hit("19-12", "crawl"=>"+", "bend"=>"", "stop"=>"+++", "hunch"=>"---", "back"=>"--"),
		Hit("NompC", "crawl"=>"+++", "bend"=>"", "stop"=>"+++", "hunch"=>"---", "back"=>""),
	]
	fig3f_expected_hits = [
		Hit("R18G08", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R22H02", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R26A08", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R30A09", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R36E08", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R41D01", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R44H08", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R50H06", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R55C12", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R58G09", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R66A12", "bend"=>"", "hunch"=>"--", "back"=>""),
		Hit("R68A08", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R73H04", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R79E02", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R87G02", "bend"=>"", "hunch"=>"---", "back"=>""),
		Hit("R13A08", "bend"=>"---", "hunch"=>"", "back"=>""),
		Hit("R13G10", "bend"=>"---", "hunch"=>"", "back"=>""),
		Hit("R31A05", "bend"=>"---", "hunch"=>"", "back"=>""),
		Hit("R47E06", "bend"=>"---", "hunch"=>"", "back"=>""),
		Hit("R58E02", "bend"=>"---", "hunch"=>"", "back"=>""),
		Hit("R77B12", "bend"=>"---", "hunch"=>"", "back"=>""),
	]
	fig3g_expected_hits = [
		Hit("R11A07", "bend"=>"", "hunch"=>"+++", "back"=>""),
		Hit("R34A10", "bend"=>"", "hunch"=>"+++", "back"=>""),
		Hit("R39H05", "bend"=>"", "hunch"=>"+++", "back"=>""),
		Hit("R48F09", "bend"=>"", "hunch"=>"+++", "back"=>""),
		Hit("R84F11", "bend"=>"", "hunch"=>"+++", "back"=>""),
		Hit("R88C11", "bend"=>"", "hunch"=>"+++", "back"=>""),
		Hit("R12F10", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R30B11", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R32E04", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R46B01", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R51F05", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R55C05", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R56E09", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R60D10", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R62A02", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R66C07", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R71C03", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R74E03", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R79C09", "bend"=>"", "hunch"=>"", "back"=>"+++"),
		Hit("R85G05", "bend"=>"", "hunch"=>"", "back"=>"+++"),
	]
	fig5a_expected_hits = [
		Hit("R11F06", "bend"=>"+++", "hunch"=>"---", "back"=>""),
		Hit("R12A02", "bend"=>"", "hunch"=>"--", "back"=>"+"),
		Hit("R15F12", "bend"=>"", "hunch"=>"-", "back"=>"+++"),
		Hit("R18E12", "bend"=>"", "hunch"=>"--", "back"=>"+"),
		Hit("R23A05", "bend"=>"---", "hunch"=>"", "back"=>"+++"),
		Hit("R23D10", "bend"=>"--", "hunch"=>"--", "back"=>"+++"),
		Hit("R24G12", "bend"=>"---", "hunch"=>"--", "back"=>"++"),
		Hit("R26D07", "bend"=>"", "hunch"=>"-", "back"=>"+++"),
		Hit("R27B12", "bend"=>"++", "hunch"=>"---", "back"=>"+"),
		Hit("R28A05", "bend"=>"", "hunch"=>"-", "back"=>"++"),
		Hit("R29D11", "bend"=>"", "hunch"=>"-", "back"=>"+++"),
		Hit("R30E06", "bend"=>"-", "hunch"=>"", "back"=>"+++"),
		Hit("R35G04", "bend"=>"-", "hunch"=>"", "back"=>"+++"),
		Hit("R36C01", "bend"=>"", "hunch"=>"--", "back"=>"+"),
		Hit("R38D07", "bend"=>"-", "hunch"=>"++", "back"=>"++"),
		Hit("R40F05", "bend"=>"", "hunch"=>"-", "back"=>"+++"),
		Hit("R45G05", "bend"=>"", "hunch"=>"---", "back"=>"+"),
		Hit("R61A01", "bend"=>"", "hunch"=>"-", "back"=>"+++"),
		Hit("R65B04", "bend"=>"+", "hunch"=>"---", "back"=>""),
		Hit("R74D08", "bend"=>"", "hunch"=>"-", "back"=>"+++"),
		Hit("R77H11", "bend"=>"", "hunch"=>"---", "back"=>"+"),
		Hit("R82B06", "bend"=>"", "hunch"=>"-", "back"=>"+++"),
		Hit("R85F12", "bend"=>"--", "hunch"=>"+", "back"=>"+"),
		Hit("R92H01", "bend"=>"-", "hunch"=>"", "back"=>"+++"),
		Hit("R95B09", "bend"=>"", "hunch"=>"--", "back"=>"++"),
	]
end;

# ╔═╡ aca6e98e-c2dc-431c-8721-64edb1a3a4bc
begin
	function comparetables(cmp, table1, table2)
		m = n = 0
		@assert length(table1) == length(table2)
		for (hit1, hit2) in zip(table1, table2)
			@assert hit1.name == hit2.name
			for (action, diff2) in pairs(hit2)
				if action == "back"
					action = "back-up"
				end
				diff1 = hit1[action]
				n += 1
				cmp(diff1, diff2) && (m += 1)
			end
		end
		return (m, n)
	end
end

# ╔═╡ f8772a0c-abc9-410f-a4f1-afa7c12c3897
begin
	function loosecompare(diff1, diff2)
		isempty(diff1) && return true
		isempty(diff2) && return true
		diff1[1] == diff2[1] && return true
		diff1[1] in ('-', 'x') && diff2[1] in ('-', 'x') && return true
		return false
	end
	
	m, n = comparetables(loosecompare, fig3e_hits, fig3e_expected_hits) .+ comparetables(loosecompare, fig3f_hits, fig3f_expected_hits) .+ comparetables(loosecompare, fig3g_hits, fig3g_expected_hits) .+ comparetables(loosecompare, fig5a_hits, fig5a_expected_hits)
end

# ╔═╡ 39abe4b5-35c4-4b71-b455-94a08e8c7f53
md"Loose matching rate: $(round(Int, (m / n) * 100))%"

# ╔═╡ e21313fb-a7de-4e8e-b600-d9a4b4b3a5dd
begin
	function strictcompare(diff1, diff2)
		isempty(diff1) && isempty(diff2) && return true
		isempty(diff1) && return false
		isempty(diff2) && return false
		diff1[1] == diff2[1] && return true
		diff1[1] in ('-', 'x') && diff2[1] in ('-', 'x') && return true
		return false
	end
	
	m′, n′ = comparetables(strictcompare, fig3e_hits, fig3e_expected_hits) .+ comparetables(strictcompare, fig3f_hits, fig3f_expected_hits) .+ comparetables(strictcompare, fig3g_hits, fig3g_expected_hits) .+ comparetables(strictcompare, fig5a_hits, fig5a_expected_hits)
end

# ╔═╡ 8c25b24f-ed72-4a60-a129-21664b569ae7
md"Strict matching rate: $(round(Int, (m′ / n′) * 100))%"

# ╔═╡ Cell order:
# ╠═10772954-bcdb-11ed-02a9-f10984510626
# ╠═7644f870-e469-4cf9-92bf-85d29140f3e3
# ╠═212563aa-5408-4e5d-8487-98f1b73d8938
# ╟─f1936c76-ad97-49b4-90c7-43b90958e1c8
# ╠═4ca60d2f-b9bb-45f5-a70d-cc15356e6c01
# ╠═b4b5a7d5-9100-4544-8a66-60c84dbf4015
# ╟─d5646f1c-353d-48fb-8e9b-097fdc3ce1ee
# ╟─17d71f68-8512-436a-84e5-4324d605f04b
# ╟─2bdce8ae-1917-40d6-a7f0-73a548390209
# ╟─81136726-b3ed-4dc0-88d0-fb7694c25cc8
# ╟─5dd5ac98-efb9-4038-9ab5-c8875d5c2a82
# ╟─011024b8-4613-444e-8f41-05b6740fd08f
# ╟─991862ea-0d96-4274-a40e-efade4bb8333
# ╠═22bc64aa-830a-4d5d-aa3d-acbf726fefde
# ╠═dfb18996-8dd3-4854-87fa-814b27836c79
# ╟─603fb1d3-af0d-4d76-a683-4797e0364b30
# ╠═73e3579d-7475-4359-900e-69289c37fd6e
# ╠═87ece16c-84db-47f3-be74-45919be4b527
# ╠═103637df-5d78-4b98-8ba7-f95ae9cd08ff
# ╠═4d6b562b-7cc5-4a7b-b0fd-71808e604518
# ╠═da5d1748-71fb-47b0-9496-4a482ef67952
# ╠═44ada27c-cf21-4e8b-875c-9ea649d8608d
# ╟─aca6e98e-c2dc-431c-8721-64edb1a3a4bc
# ╠═f8772a0c-abc9-410f-a4f1-afa7c12c3897
# ╠═39abe4b5-35c4-4b71-b455-94a08e8c7f53
# ╠═e21313fb-a7de-4e8e-b600-d9a4b4b3a5dd
# ╠═8c25b24f-ed72-4a60-a129-21664b569ae7
