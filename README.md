# t5 analysis replicates

This repository is a collection of Julia analysis scripts/notebooks that aim to reproduce results on the t5 tracker data from Janelia Research Campus, published in [Masson *et al.* 2020](https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1008589).

The original results were obtained using [J.-B. Masson's behavior tagging pipeline](https://github.com/DecBayComp/Pipeline_action_analysis_t5_pasteur_janelia). Here, we use the MaggotUBA-based [20230129 tagger](https://gitlab.pasteur.fr/nyx/MaggotUBA-adapter#anchor-20230129) instead, and a prototype 20230311 tagger trained for the 12-class behavior classification, from the [LarvaTagger project](https://gitlab.pasteur.fr/nyx/larvatagger.jl).

To deploy LarvaTagger on the Maestro HPC cluster, connect using `ssh` and run the following commands on the *maestro-submit* host:
```
cd $MYSCRATCH
git clone https://gitlab.com/larvataggerpipelines/t5_analysis_replicates
cd t5_analysis_replicates
make install
```
This downloads and converts a LarvaTagger Docker image into an Apptainer container.

To build and process the dataset in the *data/interim* directory, from the same location as above, run:
```
make
```

Analysis notebooks are available [there](https://larvataggerpipelines.gitlab.io/t5_analysis_replicates/index.html).
