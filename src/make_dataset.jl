
function crossingname(larvaline; effector="UAS_TNT_2_0003")
    if larvaline == "R84F11"
        larvaline = "R84B11"
    end
    larvaline = if larvaline[1] == 'R'
        a = if larvaline in ("R61D08", "R62A02")
            "AD"
        else
            "AE"
        end
        join(("GMR", larvaline[2:end], a, "01"), "_")
    elseif larvaline == "iav"
        "EXT_iav-GAL4"
    elseif lowercase(larvaline) == "attp2"
        "FCF_attP2_1500062"
    elseif larvaline == "19-12"
        "MZZ_R_3005998"
    elseif lowercase(larvaline) == "nompc"
        "MZZ_K_42265"
    else
        @warn "Larva line not found" larvaline
        return
    end
    return join((larvaline, effector), "@")
end

function putspinepaths!(channel, larvaline; rawdatadir="data/raw", effector="UAS_TNT_2_0003",
                        protocols=["p_8_45s1x30s0s#p_8_105s10x2s10s#n#n@100"])
    topdir = crossingname(larvaline; effector=effector)
    isnothing(topdir) && return
    ok = false
    for protocol in protocols
        parentdir = joinpath(topdir, protocol)
        path = joinpath(rawdatadir, parentdir)
        if isdir(path)
            for dir in readdir(path)
                if length(dir) == 15 && all(date_or_time -> all(isdigit, date_or_time), split(dir, '_'))
                    spinefile = join((dir, topdir, "t5", protocol), "@") * ".spine"
                    put!(channel, joinpath(parentdir, dir, spinefile))
                    ok = true
                end
            end
        end
    end
    ok || @warn "No adequate protocols" larvaline
end

function getlarvalinefiles(; datadir="data", extension=".txt")
    [file for file in readdir(datadir; join=true) if endswith(file, extension)]
end

function listspinefiles(; datadir="data", rawdatasubdir="raw", kwargs...)
    rawdatadir = joinpath(datadir, rawdatasubdir)
    Channel() do channel
        for larvalinefile in getlarvalinefiles(; datadir=datadir)
            for larvaline in eachline(larvalinefile)
                putspinepaths!(channel, larvaline; rawdatadir=rawdatadir, kwargs...)
            end
        end
    end
end

function copychorefiles(; datadir="data", srcdir="raw", destdir="interim", kwargs...)
    rawdatadir = joinpath(datadir, srcdir)
    interimdatadir = joinpath(datadir, destdir)
    for spinefile in listspinefiles(; datadir=datadir, kwargs...)
        outlinefile = spinefile[1:end-5] * "outline"
        for file in (spinefile, outlinefile)
            destfile = joinpath(interimdatadir, file)
            if !isfile(destfile)
                filename = basename(file)
                if startswith(filename, "20110715_123603")
                    file = joinpath(dirname(file), replace(filename, "53G05"=>"53g05"))
                elseif startswith(filename, "20120214_111244") || startswith(filename, "20120220_102753")
                    file = joinpath(dirname(file), replace(filename, "attP2"=>"aTTp2"))
                end
                srcfile = joinpath(rawdatadir, file)
                if isfile(srcfile)
                    dir = dirname(destfile)
                    if !isdir(dir)
                        mkpath(dir)
                    end
                    cp(srcfile, destfile)
                else
                    @warn "Cannot find file" srcfile
                end
            end
        end
    end
end

function copymatfiles(; datadir="data", srcdir="raw", destdir="interim/trx", kwargs...)
    rawdatadir = joinpath(datadir, srcdir)
    interimdatadir = joinpath(datadir, destdir)
    for spinefile in listspinefiles(; datadir=datadir, kwargs...)
        matfile = joinpath(dirname(spinefile), "trx.mat")
        srcfile = joinpath(rawdatadir, matfile)
        destfile = joinpath(interimdatadir, matfile)
        if !isfile(destfile)
            if isfile(srcfile)
                dir = dirname(destfile)
                if !isdir(dir)
                    mkpath(dir)
                end
                cp(srcfile, destfile)
            else
                @warn "Cannot find file" srcfile
            end
        end
    end
end

