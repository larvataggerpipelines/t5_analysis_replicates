SHELL:=/bin/bash
LARVATAGGER_IMAGE:=flaur/larvatagger:0.11.1-20230129

# targets (to be) exported by the host-specific makefiles
.PHONY: install init data process

all: init data process

HOST:=$(shell bin/host.sh)
export LARVATAGGER_IMAGE HOST

ifeq ($(HOST),poincare)
	include Makefile.poincare
endif

ifeq ($(HOST),maestro-submit)
	include Makefile.maestro
endif

